### TODO:
### Add charts: https://pandas.pydata.org/pandas-docs/stable/user_guide/visualization.html

#from funda_scraper import FundaScraper
import pandas as pd
import os
import logging
import sys
import datetime
import time
import re

sys.path.insert(1, '/Users/sergio.vegagutierrez/Optimizely/ai/funda-scraper/')
from funda_scraper import FundaScraper

netherlands_provincies = {
    #'Drenthe' : 'provincie-drenthe',
    #'Flevoland' : 'provincie-flevoland',
    #'Friesland' : 'provincie-friesland',
    'Gelderland' : 'provincie-gelderland',
    #'Groningen' : 'provincie-groningen',
    'Limburg' : 'provincie-limburg',
    'Noord Brabant' : 'provincie-noord-brabant',
    'Noord Holland' : 'provincie-noord-holland',
    'Overijssel' : 'provincie-overijssel',
    'Utrecht' : 'provincie-utrecht',
    'Zeeland' : 'provincie-zeeland',
    'Zuid-Holland' : 'provincie-zuid-holland'
}

def setup_logger(name, level=logging.INFO):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

def initialize_df():
    my_columns = [
      'url',
      'funda_id', 
      'city', 
      'province',
      'house_type', 
      'building_type', 
      'price', 
      #'price_m2', 
      'room', 
      #'bedroom', 
      #'bathroom', 
      'living_area_m2',
      'size_m2',
      'energy_label',
      'zip_code',
      'address',
      #'year_built',
      #'house_age', ### what is this
      #'description',
      #'photos_urls', 
      'scraped_on',
      'price_last_updated',
      'last_seen',
      ]
    df = pd.DataFrame(columns=my_columns)
    return df

def format_row(row, province):
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime("%Y-%m")
    formatted_row = pd.DataFrame({
        'url': [row.loc['url']],
        'funda_id': [row.loc['house_id']],
        'city': [row.loc['city']],
        'province' : province,
        'house_type': [row.loc['house_type']],
        'building_type': [row.loc['building_type']],
        'price': [row.loc['price']],
        #'price_m2': [row.loc['price_m2']],
        'room': [row.loc['room']],
        #'bedroom': [row.loc['bedroom']],
        #'bathroom': [row.loc['bathroom']],
        'living_area_m2': [row.loc['living_area']],
        'size_m2': [row.loc['size']],
        'energy_label': [row.loc['energy_label']],
        'zip_code': [row.loc['zip']],
        'address': [row.loc['address']],
        #'year_built': [row.loc['year_built']],
        #'house_age': [row.loc['house_age']],
        #'description': [row.loc['descrip']],
        #'photos_urls': [row.loc['photo']],
        'scraped_on' : formatted_time,
        'price_last_updated' : formatted_time,
        'last_seen' : formatted_time,
    })
    return formatted_row

def load_existing_data(path):
    if not os.path.exists(path):
        logger.info(f"No file found in {path}. Will initialize a new one")
        new_df = initialize_df()
        new_df.to_csv(path, index=False)
    else:
        logger.info(f"Found a CVS file in {path}. Will not initialize a new one")
    data = pd.read_csv(path)
    return data

def find_keyword_from_regex(x: str, pattern: str) -> int:
    result = re.findall(pattern, x)
    if len(result) > 0:
        result = "".join(result[0])
        x = result.split(" ")[0]
    else:
        x = 0
    return int(x)

def find_n_room(x: str) -> int:
    """Find the number of rooms from a string"""
    pattern = r"(\d{1,2}\s{1}kamers{0,1})|(\d{1,2}\s{1}rooms{0,1})"
    return find_keyword_from_regex(x, pattern)

def clean_price(x: str) -> int:
    """Clean the 'price' and transform from string to integer."""
    try:
        return int(str(x).split(" ")[1].replace(".", ""))
    except ValueError:
        return 0
    except IndexError:
        return 0

def clean_living_area(x: str) -> int:
    """Clean the 'living_area' and transform from string to integer"""
    try:
        return int(str(x).replace(",", "").replace(".", "").split(" m²")[0])
    except ValueError:
        return 0
    except IndexError:
        return 0

def clean_energy_label(x: str) -> str:
    """Clean the energy labels."""
    try:
        x = x.split(" ")[0]
        if x.find("A+") != -1:
            x = ">A+"
        return x
    except IndexError:
        return x

def retrieve_batch(provincie, page):
    if netherlands_provincies[provincie] is None:
        logger.critical(f"Provincie {provincie} is not valid")
    scraper = FundaScraper(area=netherlands_provincies[provincie], want_to="buy", find_past=False, n_pages=1, page_start=page)
    df = scraper.run(raw_data=True)

    ### This keeps compatibility with raw_data=False
    zip_pattern = r'\b\d{4}\s[a-zA-Z]{2}\b'
    for index, row in df.iterrows():
        url=row['url']
        df.loc[df['url'] == url, 'house_id'] = int(url.split("/")[-2].split("-")[1])
        df.loc[df['url'] == url, 'house_type'] = row['kind_of_house']
        df.loc[df['url'] == url, 'room'] = find_n_room(row['num_of_rooms'])
        zip_code = re.findall(zip_pattern, row['zip_code'])
        if len(zip_code) == 0:
          df.loc[df['url'] == url, 'zip'] = "na"
        else:
          df.loc[df['url'] == url, 'zip'] = zip_code[0]
        df.loc[df['url'] == url, 'living_area'] = clean_living_area(row['living_area'])
        cleaned_size = clean_living_area(row['size'])
        if int(cleaned_size) == 0:
            cleaned_size = clean_living_area(row['living_area'])
        df.loc[df['url'] == url, 'size'] = cleaned_size
        df.loc[df['url'] == url, 'price'] = clean_price(row['price'])
        df.loc[df['url'] == url, 'energy_label'] = clean_energy_label(row['energy_label'])

    return df

def does_house_id_exist(row, stored_df):
    house_id = row['house_id']
    if house_id in stored_df['funda_id'].values:
        logger.debug(f"--> Found in the existing CSV file. (funda_id = {house_id})")
        return True
    else:
        logger.debug(f"--> NOT Found in the existing CSV file. (funda_id = {house_id})")
        return False

def update_last_seen(funda_id, stored_df):
    current_time = datetime.datetime.now()
    formatted_time_days = current_time.strftime("%Y-%m-%d")
    stored_df.loc[stored_df['funda_id'] == funda_id, 'last_seen'] = formatted_time_days

def check_and_update_price(row, stored_df):
    new_price = row.loc['price']
    stored_price = stored_df.loc[stored_df['funda_id'] == row['house_id'], 'price'].values[0]
    if new_price == stored_price:
        logger.debug(f"--> NO new price detected. Price={new_price}. (funda_id {row['house_id']}")
    else:
        current_time = datetime.datetime.now()
        formatted_time = current_time.strftime("%Y-%m")
        logger.debug(f"--> New price detected. New price={new_price}. Old price={stored_price}. Will update. (funda_id {row['house_id']}")
        stored_df.loc[stored_df['funda_id'] == row['house_id'], 'price'] = new_price
        stored_df.loc[stored_df['funda_id'] == row['house_id'], 'price_last_updated'] = formatted_time

def process_and_save_new_batch(new_batch, csv_path, province):
    stored_df = load_existing_data(csv_path)
    new_temp_df = initialize_df()
    for index, row in new_batch.iterrows():
        logger.debug(f"Processing house ID {row['house_id']}. {row['url']}")
        if does_house_id_exist(row, stored_df):
            update_last_seen(row['house_id'], stored_df)
            check_and_update_price(row, stored_df)
        else:
            formatted_row = format_row(row, province)
            new_temp_df = pd.concat([new_temp_df, formatted_row], ignore_index=False)
    logger.debug(f"Will merge {len(new_temp_df)} new rows to the existing CSV")
    stored_df = pd.concat([stored_df, new_temp_df], ignore_index=False)
    stored_df.to_csv(csv_path, index=False)

if __name__ == '__main__': 
    logger = setup_logger('my_logger', logging.DEBUG)
    start_page = 262
    end_page = 300
    total_scrapes = len(netherlands_provincies)*(end_page+1-start_page)
    for page in range(start_page, end_page+1):
        for index, province in enumerate(netherlands_provincies.keys()):
            current_position = len(netherlands_provincies)*(page-start_page)+index
            logger.info(f"Scrapping page #{page} for province {province}. Scrape {current_position} out of {total_scrapes}")
            #raw_df = pd.read_csv('/Users/sergio.vegagutierrez/Optimizely/ai/test.csv')
            raw_df = retrieve_batch(province, page)
            process_and_save_new_batch(raw_df, '/Users/sergio.vegagutierrez/Optimizely/ai/houses_nl.csv', province)
            time.sleep(5)

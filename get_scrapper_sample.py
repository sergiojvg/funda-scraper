from funda_scraper import FundaScraper

if __name__ == '__main__': 
  scraper = FundaScraper(area="provincie-drenthe", want_to="buy", find_past=False, page_start=1, n_pages=1)
  df = scraper.run(raw_data=True, save=True, filepath="sample.csv")
  df.head()